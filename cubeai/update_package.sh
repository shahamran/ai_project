#!/usr/bin/env bash
echo "Creating package..."
python3 setup.py sdist bdist_wheel
echo "Done."
v=$(grep -oP 'version.*"\K.*(?=")' setup.py)
echo "Uploading..."
~/.local/bin/twine upload "dist/cubeai-${v}*"

